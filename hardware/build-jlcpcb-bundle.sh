#!/bin/bash

WORKING_DIR=${1:-"./gerber"}
PCB_NAME=${2:-"default"}
ACTION=${3:-"build"}
OUTPUT_PATH=$(pwd)/${PCB_NAME}-jlcpcb.zip

function _error {
  _msg=${1:-"An error occured."}
  echo "E: ${_msg}"
  exit 1
}

pushd "${WORKING_DIR}" > /dev/null

if [ "${ACTION}" == "build" ];
then
  # ensure all file exists
  echo "Checking all files are available for project ${PCB_NAME} ..."
  [ -e "${PCB_NAME}-B.Cu.gbr" ] || _error "No back copper file available."
  [ -e "${PCB_NAME}-F.Cu.gbr" ] || _error "No front copper file available."
  [ -e "${PCB_NAME}-B.Mask.gbr" ] || _error "No back mask file available."
  [ -e "${PCB_NAME}-F.Mask.gbr" ] || _error "No front mask file available."
  [ -e "${PCB_NAME}-B.SilkS.gbr" ] || _error "No back silk file available."
  [ -e "${PCB_NAME}-F.SilkS.gbr" ] || _error "No front silk file available."
  [ -e "${PCB_NAME}-Edge.Cuts.gbr" ] || _error "No edge cuts file available."
  [ -e "${PCB_NAME}.drl" ] || _error "No drill file available."

  echo "Creating JLCPCB bundle for project ${PCB_NAME} ..."

  [ -d ".jlcpcb" ] && rm -rf ".jlcpcb"
  mkdir .jlcpcb

  cp ${PCB_NAME}-B.Cu.gbr      .jlcpcb/${PCB_NAME}.GBL
  cp ${PCB_NAME}-F.Cu.gbr      .jlcpcb/${PCB_NAME}.GTL
  cp ${PCB_NAME}-B.Mask.gbr    .jlcpcb/${PCB_NAME}.GBS
  cp ${PCB_NAME}-F.Mask.gbr    .jlcpcb/${PCB_NAME}.GTS
  cp ${PCB_NAME}-B.SilkS.gbr   .jlcpcb/${PCB_NAME}.GBO
  cp ${PCB_NAME}-F.SilkS.gbr   .jlcpcb/${PCB_NAME}.GTO
  cp ${PCB_NAME}-Edge.Cuts.gbr .jlcpcb/${PCB_NAME}.GKO
  cp ${PCB_NAME}.drl           .jlcpcb/${PCB_NAME}.TXT

  zip -j ${OUTPUT_PATH} .jlcpcb/*

  rm -rf ".jlcpcb"

elif [ "${ACTION}" == "clean" ];
then
  echo "Cleaning JLCPCB bundle for project ${PCB_NAME} ..."

  rm -f ${PCB_NAME}-B.Cu.gbr
  rm -f ${PCB_NAME}-F.Cu.gbr
  rm -f ${PCB_NAME}-B.Mask.gbr
  rm -f ${PCB_NAME}-F.Mask.gbr
  rm -f ${PCB_NAME}-B.SilkS.gbr
  rm -f ${PCB_NAME}-F.SilkS.gbr
  rm -f ${PCB_NAME}-Edge.Cuts.gbr
  rm -f ${PCB_NAME}.drl

else
  _error "Unknown action: ${ACTION}"
fi

popd > /dev/null
