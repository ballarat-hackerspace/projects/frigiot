/*
**
** This file is part of Heimdall.
**
** Author(s):
**  - Ian Firns <firnsy@gmail.com>
**
** Heimdall is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** BHack Beacon is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Heimdall.  If not, see <http://www.gnu.org/licenses/>.
**
*/

//
// ANALOG / TEMP
//
// 1855 / 27 deg
// 1865 / 26 deg


#define STREAMS_DEBUG_ENABLE
#define STREAMS_JSON_ENABLE

#include <Adafruit_NeoPixel.h>

#define STREAMS_DEBUG_ENABLE
#include "BHStreams.h"

#define PIXELS   12
#define RELAY    15

#define TEMP_EVAP 33 //  2 - can't use ADC2 channel when wifi is on
#define TEMP_ROOM 34 // 25 - can't use ADC2 channel when wifi is on

#define DOOR     16

#define DEBOUNCE_INTERVAL 100
#define PIXEL_COUNT       229

#define SAMPLE_READINGS 10


Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXELS, NEO_GRB + NEO_KHZ800);
BHStreams streams;

uint32_t timeLast = 0;
uint32_t statsLast = 0;

bool relayEnabled = false;
uint32_t relayDuration = 5000;
uint32_t relayLast = 0;

bool doorOpen = false;
uint32_t doorLast = 0;

uint32_t tempSet;
uint32_t tempSetBuffer = 1;

uint32_t tempEvap = 0;
uint32_t tempRoom = 0;

uint32_t samples[SAMPLE_READINGS] = {0};
uint8_t sampleIndex = 0;
uint32_t sampleTotal = 0;
uint32_t sampleLast = 0;
uint32_t sampleAverage = 0;

double a[] = {
  -7.482589257275495e-01,
  2.891784431815734e-01,
  -3.718265512630492e-02,
  1.598120705895133e-03
};

void setup() {
  Serial.begin(115200);

  pinMode(PIXELS, OUTPUT);
  pinMode(RELAY, OUTPUT);

  pinMode(DOOR, INPUT_PULLUP);
  
  pinMode(TEMP_EVAP, INPUT);
  pinMode(TEMP_ROOM, INPUT);

  // TODO: remove with r2 board
  pinMode(2, INPUT);
  pinMode(25, INPUT);

  setPixels(255, 0, 0);

  streams.setNonBlocking(true);
  streams.begin("DWR-921-bhack", "haccadacca");
  //streams.begin("Runway Ballarat", "Runway2468!");
  
  streams.onFeed("monitor/fridge/temperature/set", handleFridgeFeed);
  streams.setConnectedCallback(handleConnected);

  streams.connect();

  //initialize the variables we're linked to
  tempSet = 2655;

  // prime samples
  sampleTotal = 0;
  for (int i=0; i<SAMPLE_READINGS; i++) {
    samples[i] = analogRead(TEMP_ROOM);
    sampleTotal += samples[i];
  }

  sampleAverage = sampleTotal / SAMPLE_READINGS;
}

void loop() {
  
  streams.process();

  uint32_t now = millis();

  if (now - sampleLast > 5000) {
    tempRoom = analogRead(TEMP_ROOM);
    // smooth sampling of the room sensor
    sampleTotal = sampleTotal - samples[sampleIndex];
    samples[sampleIndex] = tempRoom;
    sampleTotal = sampleTotal + samples[sampleIndex];
    sampleIndex = (sampleIndex + 1) % SAMPLE_READINGS;
  
    sampleAverage = sampleTotal / SAMPLE_READINGS;

    tempEvap = analogRead(TEMP_EVAP);
    Serial.printf("RAW: %d, %d\n", tempEvap, tempRoom); 
  
    Serial.printf("SET: %.02f, ROOM: %.02f, EVAP: %.02f, DOOR: %d, RELAY: %s\n", toTemp(tempSet), toTemp(sampleAverage), toTemp(tempEvap), doorOpen, (relayEnabled ? "ON" : "OFF")); 
    sampleLast = now;
  }

 if (digitalRead(DOOR) != doorOpen && now - doorLast > 500) {
    doorOpen = digitalRead(DOOR);
    doorLast = now;

    streams.write("monitor/fridge/door", doorOpen ? 1 : 0);
    Serial.printf("DOOR: %d\n", doorOpen ? 1 : 0);
  }
  
  // heartbeat
  if ((now - timeLast) > 10000) {
    timeLast = now;

    float tempNow = toTemp(sampleAverage);
    
    if (tempNow > 5.5) {
      if (!relayEnabled) {
        if (relayLast == 0 || now - relayLast > 120000) {
          relayEnabled = true;
          digitalWrite(RELAY, relayEnabled);
          Serial.printf("ROOM: %d, SET: %d, RELAY: %s\n", sampleAverage, tempSet, (relayEnabled ? "ON" : "OFF"));
          setPixels(0, 0, 255);
        }
      }
    } else if (tempNow < 2.5 && relayEnabled) {
      relayEnabled = false;
      digitalWrite(RELAY, relayEnabled);
      relayLast = now;
      Serial.printf("ROOM: %d, SET: %d, RELAY: %s\n", sampleAverage, tempSet, (relayEnabled ? "ON" : "OFF"));
      setPixels(0, 255, 0);
    }
  }
    
  if ((now - statsLast) > 60000) {
    writeStats();
    statsLast = now;
  }
}

void handleConnected(void) {
  Serial.println("STREAMS connected!");
  setPixels(255, 0, 255);
  writeStats();
}

void handleFridgeFeed(BHStreams_Data *data) {
  Serial.printf("GOT DATA: %s\n", data);
  
  double t = data->toDouble();
  double r;
  double u, v, p, q, b, c, d;
  
  t = t + 273.15;
  d = (a[0] - 1.0 / t) / a[3];
  c = a[1] / a[3];
  b = a[2] / a[3];
  q = 2.0 / 27.0 * b * b * b - 1.0 / 3.0 * b * c + d;
  p = c - 1.0 / 3.0 * b * b;
  v = - pow(q / 2.0 + sqrt(q * q / 4.0 + p * p * p / 27.0), 1.0 / 3.0);
  u =   pow(-q / 2.0 + sqrt(q * q / 4.0 + p * p * p / 27.0), 1.0 / 3.0);
  r  = exp(u + v - b / 3.0);

  Serial.printf("Aiming for: %d\n", ((int)r));
  //digitalWrite(RELAY, relayEnabled ? HIGH : LOW);
}

void setPixels(uint8_t red, uint8_t green, uint8_t blue) {
  for (uint8_t i=0; i<PIXEL_COUNT; i++) {
    strip.setPixelColor(i, strip.Color(red, green, blue));
  }

  strip.show();
}

double toTemp(uint16_t value) {
  double r = 0;

  for (int i=3; i>=0; i--) {
    r = r * log(value) + a[i];
  }

  return 1 / r - 273.15;
}

void writeStats() {
  uint32_t now = millis();

  streams.write("monitor/fridge/temperature/evap", toTemp(tempEvap));
  streams.write("monitor/fridge/temperature/room", toTemp(sampleAverage));
  
  streams.write("monitor/fridge/ip", WiFi.localIP().toString() + '\0');
  streams.write("monitor/fridge/rssi", WiFi.RSSI());
  streams.write("monitor/fridge/ts", now);
  streams.write("monitor/fridge/door", doorOpen ? 1 : 0);
}
